# Generated by Django 3.0.1 on 2019-12-20 16:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BRoast',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('boast_or_roast', models.BooleanField()),
                ('content', models.CharField(max_length=280)),
                ('upvote', models.IntegerField(blank=True, default=0)),
                ('downvote', models.IntegerField(blank=True, default=0)),
                ('time_submit', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
