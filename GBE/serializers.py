from rest_framework.serializers import HyperlinkedModelSerializer
from rest_framework import serializers
from GBE.models import BRoast

class GhostSerializer(HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    upvote = serializers.ReadOnlyField()
    downvote = serializers.ReadOnlyField()
    class Meta:
        model=BRoast
        fields=[
            'id',
            'boast_or_roast',
            'content',
            'upvote',
            'downvote',
            'time_submit',]