from django.db import models

class BRoast(models.Model):
    boast_or_roast = models.BooleanField()
    content = models.CharField(max_length=280)
    upvote = models.IntegerField(default=0, blank=True)
    downvote = models.IntegerField(default=0, blank=True)
    time_submit = models.DateTimeField(auto_now_add=True)