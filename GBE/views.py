from GBE.models import BRoast
from GBE.serializers import GhostSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from django.http import JsonResponse
from django.middleware.csrf import get_token


def csrf(request):
    return JsonResponse({'csrfToken': get_token(request)})


def ping(request):
    return JsonResponse({'result': 'OK'})


class GhostView(viewsets.ModelViewSet):
    queryset = BRoast.objects.all()
    serializer_class = GhostSerializer

    @action(detail=True, methods=['post'])
    def upvote(self, request, pk):
        broast = self.queryset.get(pk=pk)
        broast.upvote += 1
        broast.save()
        return Response({
            'status': "success",
            'id': broast.id,
            'upvotes': broast.upvote
        })

    @action(detail=True, methods=['post'])
    def downvote(self, request, pk):
        broast = self.queryset.get(pk=pk)
        broast.downvote += 1
        broast.save()
        return Response({
            'status': "success",
            'id': broast.id,
            'downvotes': broast.downvote
        })
